# -*- encoding: utf-8 -*-
#
# DATE: 2020/04/19
# AUTH: mtani@saila.co.jp
# 
# rake test:units TEST="test/unit/user_test.rb"
#

require "test_helper"

class UserTest < ActiveSupport::TestCase

  test "Userモデルの正常系 save を検証する" do
    user = User.new({
      :name => "Test User",
      :age => 99,
      :profile => "This is profile"})
    

    ret = user.save
    assert(ret, "Userモデルの DB save に失敗した")

    id = user.id
    puts "name: " + user.name
    puts "id: ", id

    new_user = User.find_by_id(id)
    puts "new user: " + new_user.name

    assert(user.name, new_user.name)

  end
end
