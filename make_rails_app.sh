#!/bin/bash
#--------------------------------------------------------------------
# DATE: 2020/04/19
# AUTH: mtani@saila.co.jp
# DESC: make rails app 
#
# userモデルを作成しサーバ(Webrick)起動まで行う
#
# gem list -d rake
# gem uninstall rake
# gem install rake -v0.8.7
#
# アプリのアクセス
# http://140.227.123.228:3000
# http://140.227.123.228:3000/users
#
#--------------------------------------------------------------------
export APP_NAME=test_app

if [ -d ${APP_NAME} ]; then
  echo "${APP_NAME} folder exists !!"
  exit 1
fi

export RAILS_ENV=development

rails new ${APP_NAME}

cd ${APP_NAME}

echo "------SCAFFOLDING"
rails generate scaffold user name:string age:integer profile:text

echo "------DB MIGRATION"
rake db:migrate

echo "------SERVER START"
rails s &


