#!/bin/bash
#------------------------------------------------------------
# DATE: 2020/04/18
# AUTH: mtani@saila.co.jp
# DESC: 
#   rbenv
#   Ruby 1.9.2p290
#   Rails 3.9.7
#   のまっさらな状態からのインストール
#
# 前提条件: sudoできる事！
#------------------------------------------------------------

#
# まず必要なものをrootでinstall
#
sudo yum install git
sudo yum install gcc make openssl-devel readline-devel
sudo yum install bzip2

git clone https://github.com/sstephenson/rbenv.git ~/.rbenv
echo 'export PATH="$HOME/.rbenv/bin:$PATH"' >> ~/.bash_profile
echo 'eval "$(rbenv init -)"' >> ~/.bash_profile
git clone https://github.com/sstephenson/ruby-build.git ~/.rbenv/plugins/ruby-build

source .bash_profile 
rbenv --version

# patch を作成

cat <<EOS>ruby1.9.2.patch
*** ext/openssl/ossl_pkey_ec.c  2015-07-10 00:18:48.945077759 +0900
--- ext/openssl/ossl_pkey_ec.c  2015-07-10 00:20:47.112714473 +0900
***************
*** 761,768 ****
--- 761,770 ----
                  method = EC_GFp_mont_method();
              } else if (id == s_GFp_nist) {
                  method = EC_GFp_nist_method();
+                #if !defined(OPENSSL_NO_EC2M)
              } else if (id == s_GF2m_simple) {
                  method = EC_GF2m_simple_method();
+                #endif
              }

              if (method) {
***************
*** 815,822 ****
--- 817,826 ----

              if (id == s_GFp) {
                  new_curve = EC_GROUP_new_curve_GFp;
+                #if !defined(OPENSSL_NO_EC2M)
              } else if (id == s_GF2m) {
                  new_curve = EC_GROUP_new_curve_GF2m;
+                #endif
              } else {
                  rb_raise(rb_eArgError, "unknown symbol, must be :GFp or :GF2m");
              }
EOS

cat ruby1.9.2.patch | rbenv install --patch 1.9.2-p290

rbenv global 1.9.2-p290

rbenv rehash

#
# railsのインストール
#
gem install -v 3.0.7 rails

rbenv rehash

echo "-------------------------------"
echo "DONE!!"
echo "-------------------------------"

