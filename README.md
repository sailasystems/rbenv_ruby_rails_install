# rbenv_ruby_rails_install

## rbenvを使ったRuby, Railsのインストールスクリプト
素のWebArena(Indigo) CentOS7.5環境
* rbenv 1.1.2-28-gc2cfbd1
* Ruby 1.9.2p290
* Rails 3.0.7
## script取得
* curl -sf https://gitlab.com/sailasystems/rbenv_ruby_rails_install/-/raw/master/rbenv_install.sh

pipeで実行するか一度ファイルに落とす

## 条件

* sudo yum install gcc make openssl-devel readline-devel
 
bitnamiをinstallして無いと以下が必要

* sudo yum install git
* sudo yum install bzip2
* opensslでエラーが出るのでpatchが必要